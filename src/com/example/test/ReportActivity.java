package com.example.test;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ReportActivity extends Activity {

	private Button buttonBack;
	private TextView textViewReport, textViewBMI;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_report);
		init();
		setTextView();
	}

	private void init() {
		buttonBack = (Button) findViewById(R.id.buttonBack);
		textViewBMI = (TextView) findViewById(R.id.textViewBMI);
		textViewReport = (TextView) findViewById(R.id.textViewReportMessage);
		buttonBack.setOnClickListener(clickListener);
	}

	private void setTextView() {
		Bundle bundle = this.getIntent().getExtras();
		String bmi = bundle.getString("BMI");
		String report = bundle.getString("REPORT");
		textViewBMI.setText(bmi);
		textViewReport.setText(report);
	}

	private Button.OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(ReportActivity.this, MainActivity.class);
			startActivity(intent);
		}
	};
}
