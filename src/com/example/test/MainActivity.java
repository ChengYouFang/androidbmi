package com.example.test;

import java.text.DecimalFormat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends Activity {
	private Button buttonTotal;
	private EditText editTextKG, editTextCM;
	private RadioGroup radioGroup;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		init();

	}

	/***
	 * 初始化元件
	 */
	private void init() {
		buttonTotal = (Button) findViewById(R.id.buttonTotal);
		editTextKG = (EditText) findViewById(R.id.editTextKG);
		editTextCM = (EditText) findViewById(R.id.editTextCM);
		radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
		buttonTotal.setOnClickListener(onClickListener);
	}

	private Button.OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// 透過try catch去捕抓沒有輸入到的例外
			try {
				double d_bmi = getBMI();
				String bmi = getBMI8(d_bmi);
				String report = getReportMessage(d_bmi);

				Intent intent = new Intent(MainActivity.this,
						ReportActivity.class);
				Bundle bundle = new Bundle();
				bundle.putString("BMI", bmi);
				bundle.putString("REPORT", report);
				intent.putExtras(bundle);
				startActivity(intent);

			} catch (NumberFormatException ex) {
				Toast.makeText(getApplicationContext(), "請輸入正確的身高以及體重", 1)
						.show();
			}
		}
	};

	/**
	 * 一般體重 理想體重 超重 嚴重超重 極度超重 體重過低
	 */
	private static final String[] REPORT_MESSAGE = { "一般體重", "理想體重", "超重",
			"嚴重超重", "極度超重", "體重過低" };

	private String getReportMessage(double bmi) {
		// 如果是男生選項打勾則為0,如果是女生選項打勾則為1
		int sex = radioGroup.getCheckedRadioButtonId() == R.id.radioMan ? 0 : 1;

		if ((sex == 0 && bmi == 24) || (sex == 1 && bmi == 22))
			return REPORT_MESSAGE[1];
		else if (bmi < 18.5)
			return REPORT_MESSAGE[5];
		else if (bmi >= 18.5 && bmi < 25)
			return REPORT_MESSAGE[0];
		else if (bmi >= 25 && bmi < 30)
			return REPORT_MESSAGE[2];
		else if (bmi >= 30 && bmi < 40)
			return REPORT_MESSAGE[3];
		else if (bmi >= 40)
			return REPORT_MESSAGE[4];

		return "";
	}

	// http://zh.wikipedia.org/zh-tw/%E8%BA%AB%E9%AB%98%E9%AB%94%E9%87%8D%E6%8C%87%E6%95%B8
	/**
	 * 取得BMI
	 * 
	 * @return BMI
	 */
	private Double getBMI() {

		double kg = Double.parseDouble(editTextKG.getText().toString());
		// 身高/100
		double cm = Double.parseDouble(editTextCM.getText().toString()) / 100.0d;
		// 體重/(身高^2)
		double bmi = kg / (cm * cm);

		return bmi;
	}

	/**
	 * 取得八位數的BMI
	 * 
	 * @param bmi
	 * @return
	 */
	private String getBMI8(double bmi) {
		DecimalFormat format = new DecimalFormat();
		// 設定浮點數最多八位
		format.setMaximumFractionDigits(8);
		// 回傳浮點數八位的BMI
		return format.format(bmi);
	}

}
